class Polygon(object):
    def __init__(self, points):
        self.points = points
        self.xs, self.ys = zip(*points)
        self.bounds = min(self.xs), min(self.ys), max(self.xs), max(self.ys)

    def make_rects(self, points):
        pass

    def fill(self, screen, ch, fg=0, bg=0):
        screen.filled_polygon(self.points, ch, fg, bg)

    def outline(self, screen, fg=0, bg=0):
        screen.polygon(self.points, fg)

class Window(object):
    def __init__(self, screen, x1, y1, x2, y2):
        self.screen = screen
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.points = [(x1, y1), (x2, y1), (x2, y2), (x1, y2)]

    def draw(self):
        self.screen.filled_polygon(self.points, ' ')
        self.screen.polygon(self.points)

