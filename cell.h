typedef struct cell {
  unsigned int ch:16;
  unsigned int fg:4;
  unsigned int bg:4;
  unsigned int bold:1;
  unsigned int ac:1;
} cell;

#define CELLNO(cells, width, pos) ((cell *)(&(cells)[(pos)]))
#define CELLIVAL(ch, fg, bg, bold, ac) \
  (*(int *)(&(cell){(ch), (fg), (bg), (bold), (ac)}))
#define SETCELL(cells, width, x, y, ch, fg, bg, bold, ac) \
  (cells)[(y) * (width) + (x)] = CELLIVAL((ch), (fg), (bg), (bold), (ac));
