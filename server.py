import socket
import select
import struct
import re
import time
import random
import traceback
import sys

import pterminal


PORT = 3000

IAC = '\xFF'
ESC = '\x1B'
DONT = '\xFe'
DO = '\xFD'
WONT = '\xFC'
WILL = '\xFB'
SB = '\xFA'
SE = '\xF0'
SEND = '\x01'

WINDOW_SIZE = '\x1F'
ECHO = '\x01'
NEW_ENVIRONMENT = '\x27'
SUPPRESS_GO_AHEAD = '\x03'
TERMINAL_TYPE = '\x18'  # RFC 1091
CHARSET = '\x2a'  # RFC 2066
BINARY = '\x00'


SPECIAL_KEYMAP = {
    'A': 'UP',
    'B': 'DOWN',
    'C': 'RIGHT',
    'D': 'LEFT',

    'H': 'HOME',
    'F': 'END',
    '3': 'DELETE', 'S': 'DELETE',
    '2': 'INSERT', 'R': 'INSERT',
}


class Telnet(object):

    def __init__(self, sock):
        self.sock = sock
        self.inbuf = ''
        self.outbuf = ''
        self.width = 0
        self.height = 0
        self.send(IAC, DO, TERMINAL_TYPE)
        self.send(IAC, SB, TERMINAL_TYPE, SEND, IAC, SE)
        self.send(IAC, DO, WINDOW_SIZE)
        self.send(IAC, WILL, BINARY)
        self.send(IAC, DO, BINARY)
        self.send(IAC, WILL, ECHO)
        self.send(IAC, WILL, SUPPRESS_GO_AHEAD)
        self.screen = None

    def send(self, *args):
        self.outbuf += ''.join(args)

    def recv(self):
        recvd = self.sock.recv(65536)
        if not recvd:
            raise Exception('disconnectered!')
        print ">", recvd.encode('hex').upper()
        self.inbuf += recvd
        while self.inbuf:
            if self.inbuf[0] == ESC:
                if len(self.inbuf) > 1 and self.inbuf[1] == 'O':
                    self.keyin(SPECIAL_KEYMAP[self.inbuf[2]])
                    self.inbuf = self.inbuf[3:]
                if len(self.inbuf) > 1 and self.inbuf[1] == '[':
                    match = re.match(r'..(.*)([a-zA-Z~])', self.inbuf)
                    if match:
                        if match.group(2) == '~':
                            self.keyin(SPECIAL_KEYMAP[match.group(1)])
                        else:
                            print "Unknown escape sequence %s %s" % \
                                (match.group(1).encode('hex'), match.group(2))
                        self.inbuf = self.inbuf[len(match.group(0)):]
            elif self.inbuf[0] == IAC:
                if len(self.inbuf) < 3:
                    break
                if self.inbuf[1] in (DONT, WONT):
                    if self.inbuf[2] == WINDOW_SIZE:
                        raise Exception("Client won't negotiate window size.")
                    self.inbuf = self.inbuf[3:]
                elif self.inbuf[1] in (DO, WILL):
                    self.inbuf = self.inbuf[3:]
                elif self.inbuf[1] == SB:
                    if SE not in self.inbuf:
                        break
                    if self.inbuf[2] == WINDOW_SIZE:
                        width, height = struct.unpack('>HH', self.inbuf[3:7])
                        self.winch(width, height)
                    elif self.inbuf[2] == TERMINAL_TYPE:
                        self.set_term(self.inbuf[4:self.inbuf.index(SE) - 1])
                    self.inbuf = self.inbuf[self.inbuf.index(SE) + 1:]
            elif self.inbuf[0] == '\x00':
                self.inbuf = self.inbuf[1:]
            else:
                self.key(self.inbuf[0])
                self.inbuf = self.inbuf[1:]

    def keyin(self, key):
        print "key ", key

    def winch(self, width, height):
        self.width = width
        self.height = height
        if self.term:
            self.window_init()

    def set_term(self, term):
        self.term = term
        if self.width and self.height:
            self.window_init()

    def flush(self):
        count = self.sock.send(self.outbuf)
        print "<", self.outbuf[:count].encode('hex').upper()
        self.outbuf = self.outbuf[count:]

    def window_init(self):
        self.screen = pterminal.Screen(self.term, self.width, self.height)
        self.send(self.screen.init())
        self.screen.box(0, 0, self.width - 1, self.height - 1, pterminal.COLOR_YELLOW)
        lava_poly = pterminal.Polygon([
            (1, 11), (11, 11), (11, 1), (21, 1), (21, 11), (31, 11),
            (31, 21), (21, 21), (21, 31), (11, 31), (11, 21), (1, 21)
        ])
        #lava_poly.outline(self.screen, pterminal.COLOR_GREEN)
        self.screen.filled_polygon(lava_poly, '@', pterminal.COLOR_YELLOW, pterminal.COLOR_RED)
        water_poly = pterminal.Polygon([
            (25, 25), (30, 25), (30, 35), (25, 35)
        ])
        self.screen.filled_polygon(water_poly, '@', pterminal.COLOR_CYAN, pterminal.COLOR_BLUE)
        self.screen.writeat(30, 5, u"\N{SNOWMAN}")
        self.send(self.screen.render())

    def close(self, *args, **kwargs):
        self.sock.close(*args, **kwargs)


def main():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    port = int(sys.argv[1]) if len(sys.argv) > 1 else PORT
    server.bind(('0.0.0.0', port))
    server.listen(5)
    print 'Listening on port %d.' % (PORT)
    client_list = []
    while True:
        inputs = [c.sock for c in client_list] + [server]
        outputs = [c.sock for c in client_list if c.outbuf]
        inputready, outputready, exceptions = select.select(inputs, outputs, inputs, 0.1)
        if server in inputready:
            client_sock, address = server.accept()
            print 'Connection from %s' % (address[0])
            client_list.append(Telnet(client_sock))
        for client in client_list:
            try:
                if client.sock in inputready:
                    client.recv()
                if client.sock in outputready:
                    client.flush()
                assert(client.sock not in exceptions)
            except:
                print "removing client for raising exception"
                try:
                    client.send("Sorry, I don't negotiate with terrorists.\n")
                except Exception:
                    pass
                traceback.print_exc()
                client.close()
                client_list.remove(client)


if __name__ == '__main__':
    main()
