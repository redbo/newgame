import sys

from libc.string cimport memcpy, strncpy
from libc.stdlib cimport calloc, free


cdef extern from "term.h":
    ctypedef struct TERMINAL:
        pass
    int setupterm(char *term, int fileno, int *err)
    void set_curterm(TERMINAL *nterm)
    int tigetflag(char *capname)
    int tigetnum(char *capname)
    char *tigetstr(char *capname)
    TERMINAL *cur_term
    char *tparm (char *, ...)
    char *tgoto (char *cstring, int hpos, int vpos)

cdef extern from "ncurses.h":
    int _COLOR_BLACK "(COLOR_BLACK+1)", _COLOR_RED "(COLOR_RED+1)", \
        _COLOR_GREEN "(COLOR_GREEN+1)", _COLOR_YELLOW "(COLOR_YELLOW+1)", \
        _COLOR_BLUE "(COLOR_BLUE+1)", _COLOR_MAGENTA "(COLOR_MAGENTA+1)", \
        _COLOR_CYAN "(COLOR_CYAN+1)", _COLOR_WHITE "(COLOR_WHITE+1)"

COLOR_BLACK, COLOR_RED, COLOR_GREEN, COLOR_YELLOW, COLOR_BLUE, \
    COLOR_MAGENTA, COLOR_CYAN, COLOR_WHITE = _COLOR_BLACK, _COLOR_RED, \
    _COLOR_GREEN, _COLOR_YELLOW, _COLOR_BLUE, _COLOR_MAGENTA, _COLOR_CYAN, \
    _COLOR_WHITE
COLOR_RESET = 0

cdef char *default_acsc = "l+m+k+j+u+t+v+w+q-x|n+o~s_`+a:f'g#~o,<+>.v-^h#"\
                          "i#0#p-r-y<z>{*|!}f"


cdef extern from "stdint.h":
    ctypedef int uint32_t

cdef extern from "alloca.h":
    void *alloca(int size)

cdef extern from "cell.h":
    ctypedef uint32_t cell_t "uint32_t"
    ctypedef struct cell:
        int ch
        int fg
        int bg
        int bold
        int ac
        int wall
    cell *CELLNO(cell_t *cells, int width, int i)
    void SETCELL(cell_t *cells, int width, int x, int y, int ch, int fg,
                int bg, int bold, int ac)
    int CELLIVAL(int ch, int fg, int bg, int bold, int ac)


cdef class Terminfo:
    cdef TERMINAL term
    cdef char *alternative_chars
    cdef object cached_attrs, acsc_map
    cdef public int LINE_HORIZ, LINE_VERT, LINE_TOPLEFT, LINE_TOPRIGHT, \
            LINE_BOTTOMLEFT, LINE_BOTTOMRIGHT, DIAMOND, BULLET, LARROW, \
            RARROW, DARROW, UARROW, PI

    def __init__(self, termtype):
        termtype = termtype.lower()
        if setupterm(termtype, -1, NULL) != 0:
            raise Exception("Unable to find terminal type: %s" % termtype)
        memcpy(&self.term, cur_term, sizeof(TERMINAL))
        self.cached_attrs = {}
        self.acsc_map = {}
        for acsc in (default_acsc, tigetstr('acsc')):
            for i in xrange(0, len(acsc), 2):
                self.acsc_map[acsc[i]] = ord(acsc[i + 1])
        self.LINE_HORIZ = self.acsc_map['x']
        self.LINE_VERT = self.acsc_map['q']
        self.LINE_TOPLEFT = self.acsc_map['l']
        self.LINE_TOPRIGHT = self.acsc_map['k']
        self.LINE_BOTTOMLEFT = self.acsc_map['m']
        self.LINE_BOTTOMRIGHT = self.acsc_map['j']
        self.DIAMOND = self.acsc_map['`']
        self.BULLET = self.acsc_map['~']
        self.LARROW = self.acsc_map[',']
        self.RARROW = self.acsc_map['+']
        self.DARROW = self.acsc_map['.']
        self.UARROW = self.acsc_map['-']
        self.PI = self.acsc_map['{']

    def tigetstr(self, attr):
        cdef char *res
        if attr not in self.cached_attrs:
            set_curterm(&self.term)
            res = tigetstr(attr)
            if res == <char *>-1 or res == NULL:
                raise Exception('Unknown term cap: %s' % attr)
            self.cached_attrs[attr] = res
        return self.cached_attrs[attr]

    # man 5 terminfo for more fun

    def goto(self, x, y):
        cm_cmd = self.tigetstr('cup')
        return tgoto(cm_cmd, x, y)

    def clear(self):
        return self.tigetstr('clear')

    def invisible_cursor(self):
        return self.tigetstr('civis')

    def normal_cursor(self):
        return self.tigetstr('cnorm')

    def delete_line(self):
        return self.tigetstr('dl1')

    def insert_line(self):
        return self.tigetstr('il1')

    def enable_alt_charset(self):
        try:
            return self.tigetstr('enacs')
        except Exception:
            return ''

    def start_alt_charset(self, char *charset):
        cap = self.tigetstr('smacs')
        return tparm(cap, charset)

    def end_alt_charset(self):
        return self.tigetstr('rmacs')

    def bold(self):
        return self.tigetstr('bold')

    def dim(self):
        return self.tigetstr('dim')

    def underline(self):
        return self.tigetstr('smul')

    def exit_underline(self):
        return self.tigetstr('rmul')

    def standard_mode(self):
        return self.tigetstr('smso')

    def disable_attributes(self):
        cap = self.tigetstr('sgr')
        return tparm(cap, 0, 0, 0, 0, 0, 0, 0, 0, 0)

    def set_attributes(self, int standout=0, int underline=0, int reverse=0,
            int blink=0, int dim=0, int bold=0, int invis=0, int protect=0,
            int altcharset=0):
        cap = self.tigetstr('sgr')
        return tparm(cap, standout, underline, reverse, blink, dim, bold,
                     invis, protect, altcharset)

    def flash(self):
        return self.tigetstr('flash')

    def name_program(self, char *name):
        cap = self.tigetstr('iprog')
        return tparm(cap, name)

    def new_line(self):
        return self.tigetstr('nel')

    def scroll_up(self):
        return self.tigetstr('ind')

    def scroll_down(self):
        return self.tigetstr('ri')

    def underline_current(self):
        return self.tigetstr('uc')

    def set_background(self, int color):
        cap = self.tigetstr('setab')
        return tparm(cap, color)

    def set_foreground(self, int color):
        cap = self.tigetstr('setaf')
        return tparm(cap, color)

    def ding(self):
        return self.tigetstr('bel')

    def move_down(self, int amt=1):
        if amt == 1:
            return self.tigetstr('cud1')
        else:
            cap = self.tigetstr('cud')
            return tparm(cap, amt)

    def move_up(self, int amt=1):
        if amt == 1:
            return self.tigetstr('cuu1')
        else:
            cap = self.tigetstr('cuu')
            return tparm(cap, amt)

    def move_left(self, int amt=1):
        if amt == 1:
            return self.tigetstr('cub1')
        else:
            cap = self.tigetstr('cub')
            return tparm(cap, amt)

    def move_right(self, int amt=1):
        if amt == 1:
            return self.tigetstr('cuf1')
        else:
            cap = self.tigetstr('cuf')
            return tparm(cap, amt)

    def scroll_forward(self):
        return self.tigetstr('ind')

    def scroll_reverse(self):
        return self.tigetstr('ri')

    def scroll_region(self):
        return self.tigetstr('csr')


_terminfo_cache = {}
def get_terminfo(term):
    if term not in _terminfo_cache:
        _terminfo_cache[term] = Terminfo(term)
    return _terminfo_cache[term]


cdef struct Vertex:
    int x
    int y
    int pos


cdef struct Rect:
    float x1, y1, x2, y2


cdef class Polygon:
    cdef Vertex *vertices
    cdef Rect *rects
    cdef int rect_count, vertex_count
    cdef int minx, miny, maxx, maxy

    def __cinit__(self, points):
        cdef int i, j, k
        self.vertex_count = len(points)
        self.vertices = <Vertex *>calloc(sizeof(Vertex), self.vertex_count)
        self.rect_count = 0
        self.rects = <Rect *>calloc(sizeof(Rect), self.vertex_count)
        self.maxx = self.minx = points[0][0]
        self.maxy = self.miny = points[0][1]

        for i in xrange(self.vertex_count):
            j = (i + 1) % self.vertex_count
            k = (i + 2) % self.vertex_count
            self.vertices[j].x = points[j][0]
            self.vertices[j].y = points[j][1]

            if self.vertices[j].x < self.minx:
                self.minx = self.vertices[j].x
            if self.vertices[j].x > self.maxx:
                self.maxx = self.vertices[j].x
            if self.vertices[j].y < self.miny:
                self.miny = self.vertices[j].y
            if self.vertices[j].y > self.maxy:
                self.maxy = self.vertices[j].y

            if (self.vertices[i].x < self.vertices[j].x and self.vertices[j].y < self.vertices[k].y) or \
                    (self.vertices[k].x < self.vertices[j].x and self.vertices[j].y < self.vertices[i].y):
                self.vertices[j].pos = 0  # TOPRIGHT
            elif (self.vertices[i].x > self.vertices[j].x and self.vertices[j].y < self.vertices[k].y) or \
                    (self.vertices[k].x > self.vertices[j].x and self.vertices[j].y < self.vertices[i].y):
                self.vertices[j].pos = 1  # TOPLEFT
            elif (self.vertices[i].x < self.vertices[j].x and self.vertices[j].y > self.vertices[k].y) or \
                    (self.vertices[k].x < self.vertices[j].x and self.vertices[j].y > self.vertices[i].y):
                self.vertices[j].pos = 2  # BOTTOMRIGHT
            elif (self.vertices[i].x > self.vertices[j].x and self.vertices[j].y > self.vertices[k].y) or \
                    (self.vertices[k].x > self.vertices[j].x and self.vertices[j].y > self.vertices[i].y):
                self.vertices[j].pos = 3  # BOTTOMLEFT

            if points[j][0] == points[0][0] or points[j][1] == points[0][1]:
                continue

            self.rects[self.rect_count].x1 = self.vertices[1].x
            self.rects[self.rect_count].x2 = self.vertices[j].x
            self.rects[self.rect_count].y1 = self.vertices[1].y
            self.rects[self.rect_count].y2 = self.vertices[j].y

            if self.vertices[j].pos == 0:
                self.rects[self.rect_count].x2 += 0.1
                self.rects[self.rect_count].y2 -= 0.1
            elif self.vertices[j].pos == 1:
                self.rects[self.rect_count].x2 -= 0.1
                self.rects[self.rect_count].y2 -= 0.1
            elif self.vertices[j].pos == 2:
                self.rects[self.rect_count].x2 += 0.1
                self.rects[self.rect_count].y2 += 0.1
            elif self.vertices[j].pos == 3:
                self.rects[self.rect_count].x2 -= 0.1
                self.rects[self.rect_count].y2 += 0.1

            if self.vertices[1].pos == 0:
                self.rects[self.rect_count].x1 += 0.1
                self.rects[self.rect_count].y1 -= 0.1
            elif self.vertices[1].pos == 1:
                self.rects[self.rect_count].x1 -= 0.1
                self.rects[self.rect_count].y1 -= 0.1
            elif self.vertices[1].pos == 2:
                self.rects[self.rect_count].x1 += 0.1
                self.rects[self.rect_count].y1 += 0.1
            elif self.vertices[1].pos == 3:
                self.rects[self.rect_count].x1 -= 0.1
                self.rects[self.rect_count].y1 += 0.1

            if self.rects[self.rect_count].x2 < self.rects[self.rect_count].x1:
                self.rects[self.rect_count].x2, self.rects[self.rect_count].x1 = \
                        self.rects[self.rect_count].x1, self.rects[self.rect_count].x2
            if self.rects[self.rect_count].y2 < self.rects[self.rect_count].y1:
                self.rects[self.rect_count].y2, self.rects[self.rect_count].y1 = \
                        self.rects[self.rect_count].y1, self.rects[self.rect_count].y2

            self.rect_count += 1

    def __dealloc__(self):
        free(self.vertices)
        free(self.rects)


cdef class Screen:
    cdef cell_t *state
    cdef cell_t *update
    cdef public int width, height, x, y
    cdef object terminfo
    cdef int _dirty

    def __cinit__(self, term, int width, int height):
        cdef index
        self.terminfo = get_terminfo(term)
        self.width = width
        self.height = height
        self.state = <cell_t *>calloc(width * height, sizeof(cell_t))
        self.update = <cell_t *>calloc(width * height, sizeof(cell_t))
        self._dirty = 0

    def __dealloc__(self):
        free(self.state)
        free(self.update)

    def init(self):
        return self.terminfo.clear() + self.terminfo.enable_alt_charset()

    def cleanup(self):
        return self.terminfo.clear() + self.terminfo.end_alt_charset()

    def setcell(self, int x, int y, int ch, int fg=0, int bg=0,
                int bold=0, int ac=0):
        SETCELL(self.update, self.width, x, y, ch, fg, bg, bold, ac)
        self._dirty = 1

    def line(self, int x1, int y1, int x2, int y2, int fg=0, int bg=0, int ch=0, int ac=1):
        if x1 == x2:
            x2 += 1
            if y2 < y1:
                y1, y2 = y2, y1
            if not ch:
                ch = self.terminfo.LINE_HORIZ
        elif y1 == y2:
            y2 += 1
            if x2 < x1:
                x1, x2 = x2, x1
            if not ch:
                ch = self.terminfo.LINE_VERT
        cdef int setto = CELLIVAL(ch, fg, bg, 0, ac)
        cdef int col, row
        for row in xrange(y1, y2):
            for col in xrange(x1, x2):
                self.update[row * self.width + col] = setto
        self._dirty = 1

    def box(self, int x, int y, int width, int height, int fg=0):
        self.polygon([(x, y), (x + width, y), (x + width, y + height), (x, y + height)], fg)

    def fill_box(self, int x1, int y1, int x2, int y2, ch, int fg=0, int bg=0, ac=0):
        cdef int setto = CELLIVAL(ord(ch), fg, bg, 0, ac), row, col
        x1, x2 = min(x1, x2), max(x1, x2)
        y1, y2 = min(y1, y2), max(y1, y2)
        for row in xrange(y1, y2):
            for col in xrange(x1, x2):
                self.update[col + row * self.width] = setto
        self._dirty = 1

    def filled_polygon(self, Polygon p, ch, int fg=0, int bg=0, ac=0):
        cdef int x, y, r
        cdef int count
        cdef int setto = CELLIVAL(ord(ch), fg, bg, 0, ac)
        for x in xrange(p.minx, p.maxx + 1):
            for y in xrange(p.miny, p.maxy + 1):
                count = 0
                for r in xrange(p.rect_count):
                    count += x > p.rects[r].x1 and x < p.rects[r].x2 and y > p.rects[r].y1 and y < p.rects[r].y2
                if count & 1:
                    self.update[x + y * self.width] = setto

    def filled_polygon2(self, points, ch, int fg=0, int bg=0, ac=0):
        cdef int length = len(points), x, y
        cdef int *xps = <int *>alloca(sizeof(int) * length)
        cdef int *yps = <int *>alloca(sizeof(int) * length)
        cdef int *rects = <int *>alloca(sizeof(int) * length * 4)
        cdef int setto = CELLIVAL(ord(ch), fg, bg, 0, ac)
        cdef int lowerx = 65536, lowery = 65536, upperx = 0, uppery = 0
        cdef int rect_count = 0, count1, count2, count3, count4
        # calculate bounding box
        for x in xrange(len(points)):
            xps[x] = points[x][0]
            yps[x] = points[x][1]
            if xps[x] < lowerx:
                lowerx = xps[x]
            if xps[x] > upperx:
                upperx = xps[x]
            if yps[x] < lowery:
                lowery = yps[x]
            if yps[x] > uppery:
                uppery = yps[x]
        # calculate all rects from first point
        for x in xrange(length):
            if xps[0] == xps[x] or yps[0] == yps[x]:
                continue
            if xps[0] < xps[x]:
                rects[rect_count * 4] = xps[0]
                rects[rect_count * 4 + 1] = xps[x]
            else:
                rects[rect_count * 4] = xps[x]
                rects[rect_count * 4 + 1] = xps[0]
            if yps[0] < yps[x]:
                rects[rect_count * 4 + 2] = yps[0]
                rects[rect_count * 4 + 3] = yps[x]
            else:
                rects[rect_count * 4 + 2] = yps[x]
                rects[rect_count * 4 + 3] = yps[0]
            rect_count += 1
        # test all points in bounding box for number of overlapping rects
        for x in xrange(lowerx, upperx + 1):
            for y in xrange(lowery, uppery + 1):
                count1 = count2 = count3 = count4 = 0
                for i in xrange(rect_count):
                    count1 ^= x > rects[i * 4] and x <= rects[i * 4 + 1] and y > rects[i * 4 + 2] and y <= rects[i * 4 + 3]
                    count2 ^= x >= rects[i * 4] and x < rects[i * 4 + 1] and y >= rects[i * 4 + 2] and y < rects[i * 4 + 3]
                    count3 ^= x >= rects[i * 4] and x < rects[i * 4 + 1] and y > rects[i * 4 + 2] and y <= rects[i * 4 + 3]
                    count4 ^= x > rects[i * 4] and x <= rects[i * 4 + 1] and y >= rects[i * 4 + 2] and y < rects[i * 4 + 3]
                if count1 | count2 | count3 | count4:
                    self.update[x + y * self.width] = setto
        self._dirty = 1

    def polygon(self, points, int fg=0):
        cdef int length = len(points), i, j, k
        cdef int *xps = <int *>alloca(sizeof(int) * length)
        cdef int *yps = <int *>alloca(sizeof(int) * length)
        for i in xrange(length):
            xps[i] = points[i][0]
            yps[i] = points[i][1]
        for i in xrange(length):
            j = (i + 1) % length
            k = (i + 2) % length
            if xps[i] == xps[j]:
                self.line(xps[i], (yps[i] if yps[i] < yps[j] else yps[j]) + 1, \
                        xps[j], yps[i] if yps[i] > yps[j] else yps[j] , fg)
            elif yps[i] == yps[j]:
                self.line((xps[i] if xps[i] < xps[j] else xps[j]) + 1, yps[i], \
                        xps[i] if xps[i] > xps[j] else xps[j], yps[j], fg)
            if (xps[i] < xps[j] and yps[j] < yps[k]) or (xps[k] < xps[j] and yps[j] < yps[i]):
                SETCELL(self.update, self.width, xps[j], yps[j], self.terminfo.LINE_TOPRIGHT, fg, 0, 0, 1)
            elif (xps[i] > xps[j] and yps[j] < yps[k]) or (xps[k] > xps[j] and yps[j] < yps[i]):
                SETCELL(self.update, self.width, xps[j], yps[j], self.terminfo.LINE_TOPLEFT, fg, 0, 0, 1)
            elif (xps[i] < xps[j] and yps[j] > yps[k]) or (xps[k] < xps[j] and yps[j] > yps[i]):
                SETCELL(self.update, self.width, xps[j], yps[j], self.terminfo.LINE_BOTTOMRIGHT, fg, 0, 0, 1)
            elif (xps[i] > xps[j] and yps[j] > yps[k]) or (xps[k] > xps[j] and yps[j] > yps[i]):
                SETCELL(self.update, self.width, xps[j], yps[j], self.terminfo.LINE_BOTTOMLEFT, fg, 0, 0, 1)
        self._dirty = 1

    def writeat(self, int x, int y, text, fg=0):
        cdef int index = 0
        for ch in text:
            SETCELL(self.update, self.width, x + index, y, ord(ch), fg, 0, 0, 0)
            index += 1
        self._dirty = 1

    def cellattrs(self, int i):
        cdef cell *curcel = CELLNO(self.update, self.width, i)
        return curcel.fg, curcel.bg, curcel.ac

    def render(self):
        cdef cell *curcel
        cdef int x, lastpos = 0, lastfg = 0, lastbg = 0, in_alt_charset = 0
        cdef int x1, y1, x2, y2
        outbuf = ''
        to_update = []

        if not self._dirty:
            return outbuf

        for x in xrange(self.width * self.height):
            if self.state[x] != self.update[x]:
                to_update.append(x)

        to_update.sort(key=self.cellattrs)

        for x in to_update:
            curcel = CELLNO(self.update, self.width, x)
            if lastpos != (x - 1):
                x1 = lastpos % self.width
                y1 = lastpos / self.width
                x2 = x % self.width
                y2 = x / self.width
                if y1 == y2:
                    if x1 < x2:
                        outbuf += self.terminfo.move_right(x2 - x1 - 1)
                    else:
                        outbuf += self.terminfo.move_left(x1 - x2 - 1)
                elif x1 == x2:
                    if y1 < y2:
                        outbuf += self.terminfo.move_up(y2 - y1 - 1)
                    else:
                        outbuf += self.terminfo.move_down(y1 - y2 - 1)
                else:
                    outbuf += self.terminfo.goto(x2, y2)
            if lastbg != curcel.bg:
                if curcel.bg == 0:
                    outbuf += self.terminfo.disable_attributes()
                    in_alt_charset = lastbg = lastfg = 0
                else:
                    outbuf += self.terminfo.set_background(curcel.bg - 1)
                    lastbg = curcel.bg
            if lastfg != curcel.fg:
                if curcel.fg == 0:
                    outbuf += self.terminfo.disable_attributes()
                    in_alt_charset = lastbg = lastfg = 0
                else:
                    outbuf += self.terminfo.set_foreground(curcel.fg - 1)
                    lastfg = curcel.fg
            if in_alt_charset and not curcel.ac:
                outbuf += self.terminfo.end_alt_charset()
                in_alt_charset = 0
            elif not in_alt_charset and curcel.ac:
                outbuf += self.terminfo.start_alt_charset('0')
                in_alt_charset = 1
            if curcel.ch > 128:
                outbuf += unichr(curcel.ch).encode('utf-8')
            else:
                outbuf += chr(curcel.ch)
            self.state[x] = self.update[x]
            lastpos = x
        if in_alt_charset:
            outbuf += self.terminfo.end_alt_charset()
        if lastfg != 0 or lastbg != 0:
            outbuf += self.terminfo.disable_attributes()
        self._dirty = 0
        sys.stdout.write("= %d\n" % len(outbuf))
        return outbuf
