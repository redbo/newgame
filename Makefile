all:
	rm -f pterminal.so
	cython pterminal.pyx
	gcc -fPIC -I/usr/include/python2.7 --shared -o pterminal.so pterminal.c -lpython2.7 -lcurses
